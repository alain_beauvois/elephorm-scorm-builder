# ToScorm
---
Ruby project to convert video formation to a scorm package repository given an XML file
For a Mac OS X UI see **[here]()**.

# Feature
* Assets can be remote or embedded if the package
* Remote assets accessibility verification
* Search assets in XML file directory if no assets directory 
given
* Duration computed if no duration specified in XML file
* Video Thumbnail generated if not found and if assets accessible
* Responsive design

## Usage

```bash
video2scorm [-f] scorm_direcoty xml_file_location [[url] assets_directory]] 
```
or
```bash
video2scorm [-f] scorm_direcoty xml_file_locatin [ur assets_directory]] 
```

#### **scorm_directory***
Scorm package directory. Must not exists.

####**-f**
Delete the given scorm package directory (if any) before operation

####**-u**
Reuse the given scorm package directory 

####**xml_file_local**
XML file path or directery where XML file must be found (in this case director must contains only one XML File).

Xml file specify chapters and units. Unit[:videoURL] must contains assets network relative or absolute path (see **[exemple](app/assets/exemples/3760141114260.xml)**).

#### **url**
Absolute url prefix of assets

If not given or equal "" assets will be embedded in scorm package.

Program abort if assets are unreacheable.

**assets_directory**
Directory of assets (medias, video, ...)

If assets directory not given, search assets in xml file directory.

## Exemples
Generate a package (assets are remotely acceded):
```bash
./video2scorm ../scorm/maya-2015 /formations/maya2015/assets/ele \
http://www.elephorm.com/formations/maya2015
```
Generate a standalone package : assets are embedded (found in XML file directory) :
```bash
./video2scorm ../scorm/maya-2015 /formations/maya2015/assets/ele
```
Generate a standalone package : assets are embeded (found in specified directory) :
```bash
./video2scorm ../scorm/maya-2015 /formations/maya2015/assets/ele "" \
my_assets_directory
```
## Scorm package distribution
The scorm directory must be zipped :
```bash
cd scorm_directory
zip -r ../scorm *
````
Zipped file can be distribued

## Build gem
gem build elephorm-scorm-builder.gemspec

## Installation on Debian

```bash
sudo apt-get install ffmpeg
rbenv
gem install bundler
git clone 
cd  toscorm
bundle install
``` 
### Installation validation

```bash
rspec
```

## Possible enhancements
* Better scroll bar
* Manage video play list history (video seen, unseen, partial and course complete)  with Scorm
* Add courses files link
* Add quizzes
* Add Player List with video
* Add Video Notes
