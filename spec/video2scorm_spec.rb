require 'byebug'
spec_path = File.dirname(__FILE__)
require 'fileutils2'
FileUtils = FileUtils2

require 'pathname'

require File.join spec_path, '..', 'lib', 'scorm_builder'


seed_dir   = File.join spec_path, 'seeds'
non_existent_scorm_dir  = File.join seed_dir, 'not_existant_scorm_dir'
non_existent_scorm_parent_dir  = File.join seed_dir, 'not_exists', 'scorm_result'
existent_scorm_dir  = File.join seed_dir, 'existant_scorm_dir'
valid_xml_file = File.join seed_dir, 'valid.xml'
invalid_xml_file = File.join seed_dir, 'invalid.xml'
non_existent_xml_file = File.join seed_dir, 'non_existent_xml_file.xml'
empty_dir = File.join seed_dir, 'empty_dir'
dir_with_two_xml_file = File.join seed_dir, 'dir_with_two_xml_files'
#file_as_dir = File.join seed_dir, 'file_as_dir'
valid_assets_dir_without_xml = File.join seed_dir, 'valid_assets_dir_without_xml' #, 'assets'
valid_assets_dir_with_xml_without_duration = File.join seed_dir, 'valid_assets_dir_with_xml_without_duration', 'assets', 'ele'
valid_assets_dir_with_xml_without_duration = File.join seed_dir, 'valid_assets_dir_with_xml_without_duration', 'assets', 'ele'
valid_flat_assets_dir = File.join seed_dir, 'valid_flat_assets_dir' 
valid_xml_with_duration = File.join seed_dir, 'valid_xml_with_duration.xml'
non_existent_assets_dir = File.join seed_dir, 'non_existent_assets'
non_existent_xml_dir = File.join seed_dir, 'non_existent_xml_dir'
xml_with_absolute=File.join seed_dir, 'with_absolute.xml'
xml_with_notfound=File.join seed_dir, 'with_notfound.xml'
assets_url = "http://questioncode.fr/elephorm/scorm/maya-2015/standalone/assets"

describe "VideoToScorm", "#args" do

    before(:each) do
        FileUtils2.rm_r  non_existent_scorm_dir if File.exists?(non_existent_scorm_dir)
        FileUtils2.rm_r  non_existent_xml_dir if File.exists?(non_existent_xml_dir)
        FileUtils2.rm    non_existent_xml_file if File.exists?(non_existent_xml_file)
        FileUtils2.rm_r  non_existent_assets_dir if File.exists?(non_existent_assets_dir)
        FileUtils2.mkdir existent_scorm_dir unless File.exists?(existent_scorm_dir)
        FileUtils2.rm_r  empty_dir if File.exists?(empty_dir)
        FileUtils2.mkdir empty_dir
    end

    it "raise an ArgumentError error when nb parameter is < 2 or > 4", case: :wrong_number_of_argument do 
        expect { ScormBuilder::VideoToScorm.new()}.to raise_error(ArgumentError, "wrong number of arguments (0 for 2..4)")
        expect { ScormBuilder::VideoToScorm.new("1")}.to raise_error(ArgumentError, "wrong number of arguments (1 for 2..4)")
        expect { ScormBuilder::VideoToScorm.new("1", "2", "3", "4", "5")}.to raise_error(ArgumentError, "wrong number of arguments (5 for 2..4)")
    end

    it "raise an AlreadyExists error if scorm  directory already exists", case: :scorm_already_exists do
        expect { 
            ScormBuilder::VideoToScorm.new(existent_scorm_dir, valid_xml_file)
        }.to raise_error(ScormBuilder::AlreadyExists)
    end

    it "raise a NotFound error if parent scorm  directory doesn't exists", case: :scorm_parent_directory_doesnt_exist do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_parent_dir, valid_xml_file)
        }.to raise_error(ScormBuilder::NotFound)
    end

    it "raise an error when no xmlfile not found", case: :xml_file_doesnt_exist do
        expect { ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, empty_dir)}.to raise_error(ScormBuilder::NotFound, /xml file not found.*/i)
        expect { ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, non_existent_xml_file)}.to raise_error(ScormBuilder::NotFound, /xml file not found.*/i)
    end

    it "raise an error when more than 1 xmlfile found", case: :xml_file_doesnt_exist do
        expect { ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, dir_with_two_xml_file)}.to raise_error(ScormBuilder::NotFound, /More than one .* xml file found.*/)
    end

    it "raise an error when invalid xml", case: :invalid_xml do
        expect { ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, invalid_xml_file)}.to raise_error(NoMethodError)
    end

    it "raise un error if haml template not found", case: :haml_template_doesnt_exist do
        current_env=ENV['TOSCORM_TEMPLATE'] 
        expect { 
                ENV['TOSCORM_TEMPLATE'] = "non_existent_template"
                #set_env_var('TOSCORM_TEMPLATE', "non_existent_template")
                ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file)
        }.to raise_error(ScormBuilder::NotFound)
        ENV['TOSCORM_TEMPLATE'] = current_env
    end

    it "raise an error if assets directory not found", case: :asset_directory_doesnt_exist do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, "", non_existent_assets_dir)
        }.to raise_error(ScormBuilder::NotFound)

        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, non_existent_assets_dir)
        }.to raise_error(ScormBuilder::NotFound)
    end

    it "raise an error if nor assets url or assets dir given and assets specified absolute path", case: :absolute_assets_url_in_but_url_given do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, xml_with_absolute, assets_url)
        }.to raise_error(ScormBuilder::NotFound, /Assets should be embedded but can't find asset because but no assets dir given and absolute assets url found in xml.*/)
    end

    it "raise an error if invalid url given", case: :scorm_already_exists do
        expect { 
            ScormBuilder::VideoToScorm.new(existent_scorm_dir, valid_xml_file, "http://nonexistantxxxxx.com")
        }.to raise_error(ScormBuilder::AlreadyExists)
        expect { 
            ScormBuilder::VideoToScorm.new(existent_scorm_dir, valid_xml_file, "http://nonexistantxxxxx")
        }.to raise_error(ScormBuilder::AlreadyExists)
        expect { 
            ScormBuilder::VideoToScorm.new(existent_scorm_dir, valid_xml_file, "nonexistantxxxxx")
        }.to raise_error(ScormBuilder::AlreadyExists)
    end

    it "raise an error if no assets directory or assets url given and asset relative_path doesn't match xml dir path", case: :media_doesnt_exist_for_standalone_package  do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file)
        }.to raise_error(ScormBuilder::NotFound, "Asset found in XML directory but its relative_path doesn't match xml dir path")
    end

    it "raise an error if assets directory given and asset relative_path doesn't match assets dir path", case: :media_doesnt_exist_for_standalone_package  do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_with_duration, "", valid_flat_assets_dir)
        }.to raise_error(ScormBuilder::NotFound, "Asset found in assets directory but its relative_path doesn't match assets dir path")
        FileUtils2.rm_r  non_existent_scorm_dir if File.exists? non_existent_scorm_dir 
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, Pathname.new(valid_xml_file).parent.to_s)
        }.to raise_error(ScormBuilder::NotFound, "Asset found in assets directory but its relative_path doesn't match assets dir path")
    end

    it "should raise an error if assets dir specified but media not found", :asset_not_found_in_assets_directory  do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, xml_with_notfound, valid_assets_dir_without_xml)
        }.to raise_error(ScormBuilder::NotFound)
    end


    it "should be ok if assets url given but relative path of asset url and assets directory mismatch", case: :good_configuration do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_assets_dir_with_xml_without_duration, assets_url)
        }.not_to raise_error

        expect(File).to exist(non_existent_scorm_dir)
        expect(File).to exist(File.join(non_existent_scorm_dir, 'index.html'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'images'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'javascripts'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'stylesheets'))
    end

    it "should be ok if assets_path specified", case: :assets_found_in_given_assets_directory do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, "", valid_assets_dir_without_xml)
        }.not_to raise_error
        FileUtils2.rm_r  non_existent_scorm_dir if File.exist? non_existent_scorm_dir 
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, valid_assets_dir_without_xml)
        }.not_to raise_error

        expect(File).to exist(non_existent_scorm_dir)
        expect(File).to exist(File.join(non_existent_scorm_dir, 'index.html'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'images'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'javascripts'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'stylesheets'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'fonts'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'assets'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'config.xml'))
        #COUNT : count file count
    end

    it "should be ok if no assets directory or assets url given and asset relative_path doesn't match xml dir path", case: :ok_if_assets_relative_path_doesnt_match_url_relative_path  do
        expect { 
            ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_with_duration, assets_url, valid_assets_dir_without_xml)
        }.not_to raise_error
    end

    it "should be ok if no url or media_path specified", tagged: :assets_found_in_xml_file_directory do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_assets_dir_with_xml_without_duration) 
        }.not_to raise_error

        expect(File).to exist(non_existent_scorm_dir)
        expect(File).to exist(File.join(non_existent_scorm_dir, 'index.html'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'images'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'javascripts'))
        expect(File).to exist(File.join(non_existent_scorm_dir, 'stylesheets'))
    end

    it "should not create a scorm assets directory if url given" do
          expect { 
              ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_with_duration, assets_url)
          }.not_to raise_error
          expect(
              File.exists?(File.join(non_existent_scorm_dir, 'assets'))
          ).to be false
    end

    it "should be ok if non duration", case: :missing_duration_information_can_be_retrieve_from_asset_analyzing do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_assets_dir_with_xml_without_duration, assets_url,)
        }.not_to raise_error
        FileUtils2.rm_r  non_existent_scorm_dir if File.exist? non_existent_scorm_dir 
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_file, valid_assets_dir_without_xml)
        }.not_to raise_error

    end

    it "should be ok if with duration", case:  :ok_if_duration_in_xml do
        expect {
          ScormBuilder::VideoToScorm.new(non_existent_scorm_dir, valid_xml_with_duration, assets_url)
        }.not_to raise_error
    end

    after do
        FileUtils2.rm_r  non_existent_scorm_dir if File.exists?(non_existent_scorm_dir)
        FileUtils2.rm_r  non_existent_xml_dir if File.exists?(non_existent_xml_dir)
        FileUtils2.rm    non_existent_xml_file if File.exists?(non_existent_xml_file)
        FileUtils2.rm_r  non_existent_assets_dir if File.exists?(non_existent_assets_dir)
        FileUtils2.mkdir existent_scorm_dir unless File.exists?(existent_scorm_dir)
        FileUtils2.rm_r  empty_dir if File.exists?(empty_dir)
        FileUtils2.mkdir empty_dir
    end

end
