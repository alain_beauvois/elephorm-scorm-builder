class String
        def blank?
              respond_to?(:empty?) ? !!empty? : !self
        end
        def starts_with?(prefix)
            prefix.respond_to?(:to_str) && self[0, prefix.length] == prefix
        end
end
class NilClass
    def blank?
        return true
    end
end

module ScormBuilder
    class Error < RuntimeError
    end

    class NotFound < Error
    end

    class InvalidParameter < Error
    end

    class AlreadyExists < Error
    end

    class InvalidDirectory < Error
    end

    class XMLError < Error
    end

    # Test if a string is basically an integer in quotes 

    # Create a scorm package blank  
    # a scorm directory
    # a xml file
    # an optional assets directory or an option assets url 
    class VideoToScorm
        # IMPORTANT NOTICE FOR DEVELOPPERS
        # Use notation with symbol (eg. unit[:videoURL] to change XML field value -- in order to avoid XML field duplication
        require 'haml'
        require 'ox'
        #require 'byebug'
        require 'fileutils2'
        FileUtils = FileUtils2
        require 'pathname'
        require 'streamio-ffmpeg'
        require 'mustache'
        require 'net/http'

        attr_accessor :ox
        attr_accessor :scorm_html_filename
        attr_accessor :haml_template_file
        attr_accessor :scorm_directory
        attr_accessor :xml_file
        attr_accessor :assets_directory
        attr_accessor :media_url
        attr_accessor :root_path

        # constructor
        def initialize(*args)
            if args.length > 1 
                if [ '-f', '--force' ].include? args[0]
                    FileUtils.rm_r(args[1]) if (File.exists?(args[1]) && File.directory?(args[1]))
                    args.shift
                elsif [ '-u', '--reuse' ].include? args[0]
                    @updated = true
                    args.shift
                else
                    @updated = false
                end
            end
            initialize2(*args)
        end

        def initialize2(scorm_directory, xml_file, assets_url = "", assets_directory = "")
            raise AlreadyExists, "Directory #{scorm_directory} already exists" if File.exists?(scorm_directory) && !@updated
            @scorm_directory = scorm_directory
            if assets_directory.blank? && !assets_url.blank?
                unless assets_url.starts_with?('http') || assets_url.starts_with?('//')
                    assets_directory  = assets_url
                    assets_url = ""
                end
            end

            @assets_url = assets_url
            @assets_directory = assets_directory

            scorm_parent_directory = File.expand_path("..",scorm_directory)
            raise NotFound, "scorm parent director is not a directory : #{scorm_parent_directory}" unless File.directory?(scorm_parent_directory)

            @root_path = File.expand_path("..",File.dirname(__FILE__))

            @scorm_directory = scorm_directory
            @scorm_assets_directory = 'assets'
            require File.join(@root_path, 'config', "video2scorm.rb")

            @xml_file = xml_file

            if File.directory?(xml_file)
                @xml_directory = xml_file
                arr = Dir.glob("#{@xml_directory}/*.xml")
                raise NotFound, "More than one (#{arr.length}) xml file found in #{@xml_directory}" if arr.length > 1
                raise NotFound,  "xml file not found in #{@xml_directory}" if arr.length == 0 
                @xml_file = arr[0]
            elsif File.exists?(xml_file)
                @xml_file = xml_file
                @xml_directory = File.expand_path("..", xml_file)
            else
                raise NotFound, "xml file not found : #{xml_file}"
            end

            if assets_directory.empty?  
                a2 = Dir.glob("#{@xml_directory}/*.mp4")
                @xml_directory_has_assets = true if !a2.nil? && a2.length > 0
            else 
                raise NotFound, "Assets directory not found : #{assets_directory}"  unless File.exists?(assets_directory) && File.directory?(assets_directory)
            end

            @haml_template_file = ENV['TOSCORM_TEMPLATE'].blank?  ? 'template' : ENV['TOSCORM_TEMPLATE']
            puts "TOSCORM_TEMPLATE #{ENV['TOSCORM_TEMPLATE']}" unless ENV['TOSCROP_TEMPLATE'].blank?
            @haml_template_file = "#{@haml_template_file}.html.haml"

            @haml_template_filepath = File.join(@root_path, 'app', 'views', 'video2scorm', @haml_template_file)
            raise NotFound, "HAML template file not found (@haml_template_file) : #{@haml_template_filepath}" unless File.exists?(@haml_template_filepath)

            @scorm_html_filename = ENV['TOSCORM_HTMLFILENAME'].blank?  ? 'index.html' : ENV['TOSCORM_HTMLFILENAME']
            @scorm_html_filepath = File.join(@scorm_directory, @scorm_html_filename)

            @manifest_file = ENV['TOSCORM_MANIFEST'].blank?  ? 'imsmanifest' : ENV['TOSCORM_MANIFEST']

            @ox = Ox.load_file(@xml_file)


            FileUtils.mkdir @scorm_directory unless Dir.exists?(@scorm_directory)
            FileUtils.mkdir_p File.join(@scorm_directory, @scorm_assets_directory) if @assets_url.blank? 

            copy_assets_into_scorm_directory
            File.write File.join(@scorm_directory, 'config.xml'), Ox.dump(@ox)

            generate_html(@haml_template_filepath)

            generate_scorm_manifest

        end

        private

        def generate_scorm_manifest
            manifest = File.read(File.join(@root_path, 'app', 'xml', 'video2scorm', "#{@manifest_file}.xml"))
            #@manifest = Ox.load_file(File.join(@root_path, 'app', 'xml', 'video2scorm', "#{@manifest_file}.xml"))
            xml = Mustache.render(manifest, title: @ox.title)
            File.write File.join(@scorm_directory, 'imsmanifest.xml'), xml 
        end

        # generat html for the scorm package
        # (javascripts, css, images and html)
        def generate_html(haml_template_filepath)

            haml = File.read(haml_template_filepath)
            engine = Haml::Engine.new(haml)
            #Ox.to_file('/tmp/xxx.xml', @ox)
            #FileUtils.rm_r(Dir.glob("#{@scorm_directory}"))

            FileUtils.rm_r File.join(@scorm_directory, "javascripts") if File.exists? File.join(@scorm_directory, "javascripts")
            FileUtils.rm_r File.join(@scorm_directory, "stylesheets") if File.exists? File.join(@scorm_directory, "stylesheets")
            FileUtils.rm_r File.join(@scorm_directory, "images") if File.exists? File.join(@scorm_directory, "images")
            FileUtils.rm_r File.join(@scorm_directory, "fonts") if File.exists? File.join(@scorm_directory, "fonts")

            FileUtils.cp_r "#{@root_path}/app/assets/javascripts/video2scorm", "#{@scorm_directory}/javascripts"
            FileUtils.cp_r "#{@root_path}/app/assets/stylesheets/video2scorm", "#{@scorm_directory}/stylesheets"
            FileUtils.cp_r "#{@root_path}/app/assets/images/video2scorm", "#{@scorm_directory}/images"
            FileUtils.cp_r "#{@root_path}/app/assets/fonts", "#{@scorm_directory}/fonts"
            html = engine.render(@ox, {:ox => @ox, :scorm_builder => self})
            File.write("#{@scorm_html_filepath}", html)
        end

        # copy assets into the scorm package unless assets url blank
        # compute video duration if needed
        # compute total duration
        # create thumbnail if needed 
        def copy_assets_into_scorm_directory
            total_duration = 0
            has_duration = true
            index = 0
            for  chapter in @ox.nodes
                for unit in chapter.nodes
                    unit[:index] = index
                    index = index + 1

                    unless unit[:videoURL].blank? || unit[:videoURL].starts_with?('http') || unit[:videoURL].starts_with?('//')
                        unit[:asset_path] = search_asset_file_path_of(unit[:videoURL])
                        raise NotFound, "No assets path found for :#{unit[:asset_path]}" if unit[:asset_path].blank? && @assets_url.blank?
                        unit[:asset_thumbnail_path] = search_asset_file_path_of(unit[:thumbnail]) unless unit[:thumbnail].blank?
                        unit[:duration] = duration_of(unit)
                    end

                    unit_is_remote = true if (unit.videoURL.starts_with?('http')  || unit.videoURL.starts_with?('//')) 
                    asset_is_remote = true if unit_is_remote || !@assets_url.empty?
                    if asset_is_remote
                        raise NotFound, "Assets should be embedded but can't find asset because but no assets dir given and absolute assets url found in xml: (#{unit[:videoURL]})" if unit_is_remote && !@assets_url.empty?  
                        unit[:absolute_url] = absolute_url_of(unit[:videoURL])
                        if unit[:thumbnail].blank?
                            dirname  = parent_dir_path(unit[:videoURL])
                            basename = "#{File.basename(unit[:videoURL], ".*")}.jpg"
                            unit[:thumbnail] = "#{dirname}/#{basename}"
                        end
                        unit[:absolute_thumbnail_url] = absolute_url_of(unit[:thumbnail])
                    end
                    m1 =  @assets_url.empty?  ? "no_assets_url" : "assets_url"
                    if  unit[:videoURL].empty?
                        m2 = "no_unit_url"
                    elsif unit_is_remote
                        m2 = "unit_is_remote" 
                    else
                        m2 = "unit_relative_url" 
                    end
                    method_name = "#{m1}_and_#{m2}"
                    send(method_name.to_sym, unit)

                    # copy asset in scrom directory
                    unless asset_is_remote
                        to_asset = File.join(@scorm_directory, @scorm_assets_directory,unit[:videoURL])
                        to_asset_dir = parent_dir_path(to_asset)
                        FileUtils.mkdir_p to_asset_dir unless File.exists?(to_asset_dir) 
                        unless File.exists?(to_asset)
                            #puts "cp #{unit[:asset_path]} #{to_asset}"
                            FileUtils.cp(unit[:asset_path], to_asset)
                        end
                        # copy or generate thumbnail in scrom directory
                        if !defined?(unit[:thumbnail]) || unit[:thumbnail].empty? || unit[:asset_thumbnail_path].blank?
                            basename = "#{File.basename(unit[:videoURL], ".*")}.jpg"
                            unit[:thumbnail] = File.join(parent_dir_path(unit[:videoURL]), basename)
                            to_thumbnail = File.join(@scorm_directory, @scorm_assets_directory,unit[:thumbnail])
                            unless File.exists?(to_thumbnail)
                                movie = FFMPEG::Movie.new(unit[:asset_path])
                                movie.screenshot(to_thumbnail, {seek_time: 1, custom: '-loglevel warning'})
                                raise NotFound, "No thumbnail file found (#{to_thumbnail})" unless File.exists?(to_thumbnail)
                            end
                        else
                            to_thumbnail = File.join(@scorm_directory, @scorm_assets_directory,unit[:thumbnail])
                            to_thumbnail_dir = parent_dir_path(to_thumbnail)
                            FileUtils.mkdir_p to_thumbnail_dir  unless File.exists?(to_thumbnail_dir)
                            unless File.exists?(to_thumbnail)
                                #puts "cp #{unit[:asset_thumbnail_path]} #{to_thumbnail}"
                                FileUtils.cp(unit[:asset_thumbnail_path], to_thumbnail)
                            end
                        end
                        unit[:videoURL]  = "#{@scorm_assets_directory}/#{unit[:videoURL]}" 
                        unit[:thumbnail] = "#{@scorm_assets_directory}/#{unit[:thumbnail]}"
                    end

                    # calculate total duration 
                    unit[:seconds] = seconds = self.class.string_to_seconds(unit[:duration])
                    total_duration += seconds

                    unit[:videoURL] = remote_url_of(unit[:videoURL])
                    unit[:thumbnail] = remote_url_of(unit[:thumbnail])
                end
            end
            if has_duration
                @ox[:seconds] = total_duration
                @ox[:duration] = self.class.seconds_to_string(total_duration)
            end

        end

        # generate duration if needed
        def duration_of unit
            return unit[:duration] unless unit[:duration].blank?
            unless unit[:asset_path].empty?
                movie = FFMPEG::Movie.new(unit[:asset_path])
                raise Error, "Movie can't be read : #{unit[:asset_path]}" unless movie.valid?
                seconds = seconds = movie.duration.floor
                return self.class.seconds_to_string(seconds)
            end

        end

        def assets_url_and_no_unit_url unit
            raise XMLError, "Asset url blank but not duration found in XML File" if unit[:duration].empty?
        end

        def assets_url_and_unit_is_remote unit
            raise XMLError, "asset url blank but absolute url (#{unit[:videoURL]}) found in XML file"
        end

        def assets_url_and_unit_relative_url unit
            raise XMLError, "Asset url blank but not duration or asset_path found in XML" if unit[:duration].empty? 
            #raise XMLError, "Asset url blank but not thumbnail url or asset_path found in XML" if unit[:thumbnail].empty?
        end

        def no_assets_url_and_no_unit_url unit
            raise XMLError, "No assets path found in XML File" if unit[:videoURL].blank?
        end

        def no_assets_url_and_unit_is_remote unit
            raise XMLError, "Asset url blank but not duration found in XML File" if unit[:duration].empty?
        end

        def no_assets_url_and_unit_relative_url unit
            raise XMLError, "Asset url blank but not duration found or asset_path found in XML" if unit[:duration].empty?
        end

        def remote_url_of(url)
            return url if @assets_url.empty? || url.starts_with?('http') || url.starts_with?('//')
            if url.starts_with?('/')
                new_url ="#{@assets_url}#{url}" 
            else
                new_url = "#{@assets_url}/#{url}" if  !@assets_url.empty?
            end
            new_url
        end

        def search_asset_file_path_of(asset_url)
            # if not in assets_dir search in xml_directory
            #path = File.join(base_directory_of(asset), asset)

            # search in asset_directory
            unless @assets_directory.blank?
                path = File.join(@assets_directory, asset_url)  
                return path if File.exists?(path)
                path = search_numbered_asset(File.join(@assets_directory, asset_url))
                return path if !path.nil? && File.exists?(path)
            end

            # search in xml_directoryectory
            path = File.join(@xml_directory, asset_url)  

            return path if File.exists?(path)
            path = search_numbered_asset(File.join(@xml_directory, asset_url))
            return path if !path.nil? && File.exists?(path)

            # search in parent of xml directory
            path  = ancestor_path(@xml_directory, asset_url)
            return path if File.exists?(path)
            path = search_numbered_asset(path)
            return path if !path.nil? && File.exists?(path)


            # search in parent of @assets_directory
            unless @assets_directory.blank?
                path  = ancestor_path(@assets_directory, asset_url)
                return path if File.exists?(path)
                path = search_numbered_asset(path)
                return path if !path.nil? && File.exists?(path)
                path = File.join(@assets_directory, File.basename(asset_url))
                path = search_numbered_asset(path) unless File.exists?(path)
                if !path.nil? && File.exists?(path)
                    return path if !@assets_url.blank?
                    raise NotFound, "Asset found in assets directory but its relative_path doesn't match assets dir path" 
                end
            end

            path = File.join(@xml_directory, File.basename(asset_url))
            if File.exists?(path)
                return path if !@assets_url.blank?
                raise NotFound, "Asset found in XML directory but its relative_path doesn't match xml dir path" 
            end

        end


        def ancestor_path(relative_dir, url)
            # if url has n directory, change search dir up n times
            nbdir = url.split('/').length
            dir = Pathname.new(relative_dir)
            while nbdir > 1 do
                dir = dir.parent
                nbdir = nbdir - 1
            end
            File.join(dir, url)
        end

        def search_numbered_asset url
            # if video file not found, and if video name begin with n-.*, find 
            # search for a file numbered n-.* (only one file must exists!)
            basename = get_basename(url)
            dirname = get_dirname(url)
            a =  basename.split('-')[0]
            unless a.blank? && a.is_i?
                a2 = Dir.glob("#{dirname}/#{a}-*")
                if a2.length == 1
                    return a2[0]
                end
            end
            return nil
        end

        # return  the last part of an url
        def get_basename(url)
            arr = url.split('/')
            arr[arr.length - 1]
        end

        def get_dirname(url)
            arr = url.split('/')
            i = 0
            dirname=""
            while i < arr.length - 1
                dirname="#{dirname}#{arr[i]}"
                i = i + 1
                dirname="#{dirname}/" if (i != (arr.length - 1))
            end
            dirname
        end

        def self.get_dirname(url)
            arr = url.split('/')
            i = 0
            dirname=""
            while i < arr.length - 1
                dirname="#{dirname}#{arr[i]}"
                i = i + 1
                dirname="#{dirname}/" if (i != (arr.length - 1))
            end
            dirname
        end

        def absolute_url_of(url)
           absolute_url = remote_url_of(url)
           validates_url_of absolute_url
           absolute_url
        end

        def validates_url_of absolute_url
            raise Error, "URL must start with \"http\" or \"//\" : #{absolute_url}" unless  (absolute_url.starts_with?('http') || absolute_url.starts_with?('//'))
            url = URI.parse(absolute_url)
            req = Net::HTTP.new(url.host, url.port)
            if url.scheme == "https"
                req.use_ssl = true if url.scheme == "https"
                req.verify_mode = OpenSSL::SSL::VERIFY_NONE
            end
            res = req.request_head(url)
            raise Error, "Network URL #{absolute_url} doesn't respond" if res.code != "200" && absolute_url.end_with?('mp4')
        end

        public

        def parent_dir_path url
            return  Pathname.new(url).dirname.sub(/\.$/,'')
        end

        def self.seconds_to_string(duration)
            if duration >= 3600 
                duration_hh = (duration / 3600).floor
                duration_mm = ((duration - (duration_hh * 3600)) / 60).floor
            else
                duration_hh = 0
                duration_mm = (duration / 60).floor
            end
            duration_ss = duration - (duration_hh * 3600) - (duration_mm * 60)
            hh = "%02d" % duration_hh 
            mm = "%02d" % duration_mm  
            ss = "%02d" % duration_ss
            duration >= 3600 ?  "#{hh}:#{mm}:#{ss}" : "#{mm}:#{ss}"
        end

        # convert "hh:mm:ss" or "mm:ss" to a number of seconds (integer)
        def self.string_to_seconds(duration) 
            arr =duration.split(':')
            arr.length == 3 ? ((arr[0].to_i * 3600) + (arr[1].to_i * 60) + arr[2].to_i)  :  ((arr[0].to_i * 60) + arr[1].to_i)
        end
    end
end
