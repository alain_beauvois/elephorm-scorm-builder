function alert(text) {
    var el = document.getElementById('main-message');
    if (typeof el.textContent != 'undefined') {
        el.textContent = text;
    }
    else {
        el.innerText = text;
    }
    el.className = "error";
}

(function() {
        var mainPlayer;

        function textContent(el, text) { //for compatibility IE < 9
            if (typeof el.textContent != 'undefined') {
                el.textContent = text;
            }
            else {
                el.innerText = text;
            }
        }
        window.addEventListener('load', function(event) {
            var mainPlayer = videojs('main-video');
            myScorm.initialize();
            var backBtnClick = function(event) {
                event.preventDefault();
                mainPlayer.pause();
                document.body.className="display-section-summary";
                return false;
            };
            var videoLinkClick = function(event) {
                event.preventDefault();
                var url = event.target.attributes['data-url'].value;
                if (!url) return false;

                textContent(H1Video, event.target.innerHTML);
                var poster = event.target.getAttribute('data-thumbnail');
                mainPlayer.src([{type: "video/mp4", src: url, poster: poster}]);
                mainPlayer.posterImage.show();

                document.body.className="display-section-video";
                manageVideo(poster);
                return false;
            };
            var getWindowSize = function() {
                var w = window,
                d = document,
                e = d.documentElement,
                g = d.getElementsByTagName('body')[0],
                x = w.innerWidth || e.clientWidth || g.clientWidth,
                y = w.innerHeight|| e.clientHeight|| g.clientHeight;
                return { width: x, height: x};
            };
            var manageVideo = function() {
                videojs("main-video").ready(function(){

                    var myPlayer = this;    // Store the video object
                    //var aspectRatio = 9/16; // Make up an aspect ratio
                    var aspectRatio = 800/1280; // (10/16)

                    function resizeVideoJS(){
                        // Get the parent element's actual width

                        var parentElement = document.getElementById("main-video").parentElement;
                        var width = parentElement.offsetWidth;
                        var height = parentElement.offsetHeight;
                        var vHeight = width * aspectRatio; 
                        var figCaption = parentElement.getElementsByTagName('figcaption')[0];
                        if (vHeight > height) { 
                            myPlayer.width(height / aspectRatio).height( height );
                            if (figCaption) figCaption.style.width = (height/aspectRatio) + 'px';
                        }
                        else {
                            // Set width to fill parent element, Set height
                            myPlayer.width(width).height( width * aspectRatio );
                            if (figCaption) figCaption.style.width = width + 'px';
                        }
                    }


                    resizeVideoJS(); // Initialize the function
                    mainPlayer.play();
                    window.onresize = resizeVideoJS; // Call the function on resize
                });
            };

            //var sectionSummary=document.getElementsById('section-summary')

            var sectionSummary=document.getElementById('section-summary');
            var sectionVideo=document.getElementById('section-video');

            var mainVideo=document.getElementById('main-video').getElementsByTagName('video')[0];
            var H1Video = sectionVideo.getElementsByTagName('h1')[0];

            var i;
            var videoLinks = document.getElementsByClassName('video-link');
            for (i = 0; i < videoLinks.length; i++) videoLinks[i].addEventListener('click', videoLinkClick); 

            var backBtns=document.getElementsByClassName('back-to-summary');
            for (i = 0; i < backBtns.length; i++) backBtns[i].addEventListener('click', backBtnClick);

        });
})();
