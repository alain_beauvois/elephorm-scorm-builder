// see http://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/
//   cmi.core.session_time (CMITimespan, WO) Amount of time that the learner has spent in the current learner session for this SCO
//   cmi.suspend_data (CMIString (SPM: 4096), RW) Provides space to store and retrieve data between learner sessions
//   cmi.launch_data (CMIString (SPM: 4096), RO) Data provided to a SCO after launch, initialized from the dataFromLMS manifest element
//   cmi.comments (CMIString (SPM: 4096), RW) Textual input from the learner about the SCO
var myScorm = {
    initialize: function() {
        var result = doLMSInitialize();
        if (result == "true") 
        {
            myScorm.complete(100); // set to complete once loaded (FOR NOW)
        }
    },
    getCurrentActivity: function() {
      return doLMGetValue('cmi.location'); // restore current lesson
    },
    setCurrentActivity: function(id) {
       doLMSSetValue('cmi.location', id);
    },
    setEndOfActivy: function(id) {
    },
    setActivyComplete: function(id) {
    },
    onExit: function() {
        // cmi.core.lesson_status (“passed”, “completed”, “failed”, “incomplete”, “browsed”, “not attempted”, RW) 
        // TODO ON Exit : mark this activity has completed if everything done
        doLMSCommit();
    },
    setLessonStatus: function(id, status ) {
      //'cmi.core.lesson_location', id
      // cmi.core.lesson_status' “passed”, “completed”, “failed”, “incomplete”, “browsed”, “not attempted”
      doLMSSetValue('cmi.core.lesson_status', status);
    },
    complete: function(score) {

        doLMSSetValue("cmi.core.score.raw", score);
        doLMSSetValue("cmi.core.lesson_status", "completed");
        doLMSCommit();
    }
};
