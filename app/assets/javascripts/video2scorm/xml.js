function importXML(xmlfile, callback)
{
    var xmlDoc;
    var xmlloaded = false;
    var xmlhttp;
    try
    {
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", xmlfile, false);
    }
    catch (Exception)
    {
        var ie = (typeof window.ActiveXObject != 'undefined');

        if (ie)
        {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            while(xmlDoc.readyState != 4) {}
            xmlDoc.load(xmlfile);
            //readXML();
            if (callback) {
                callback(xmlDoc);
            }
            xmlloaded = true;
        }
        else
        {
            xmlDoc = document.implementation.createDocument("", "", null);
            xmlDoc.onload = function() {
                if (callback) {
                    callback(xmlDoc);
                }
            };
            xmlDoc.load(xmlfile);
            xmlloaded = true;
        }
    }

    if (!xmlloaded)
    {
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send("");
        xmlDoc = xmlhttp.responseXML;
        if (callback) {
            callback(xmlDoc);
        }
        //readXML();
        xmlloaded = true;
    }
}
