# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name        = 'elephorm-scorm-builder'
  s.version     = '0.0.8'
  s.date        = '2010-04-28'
  s.summary     = "Build a scorm package form an XML file!"
  s.description = "A simple hello world gem"
  s.authors     = ["Alain Beauvois"]
  s.email       = 'admin@questioncode.fr'

  s.files       = `git ls-files | grep -v elephorm-scorm-builder.*.gem`.split("\n")
  s.test_files  = `git ls-files -- spec/*`.split("\n")
  #s.executables = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.executables = 'video2scorm'
  s.require_paths = ["lib"]


  s.add_dependency "haml", "4.0.5"
  s.add_dependency "ox", "2.1.3"
  s.add_dependency "byebug", "3.5.1"
  s.add_dependency "streamio-ffmpeg", "1.0.0"
  s.add_dependency "mustache", "0.99.7"
  s.add_dependency "fileutils2", "0.2.0"
  s.add_dependency "rake", "10.3.2"
  s.add_development_dependency "rspec", "4.0.5"

  s.homepage    = 'https://bitbucket.org/alain_beauvois/elephorm-scorm-builder'
  s.license     = 'Elephorm'
end
